const DBManager = ({onDBInit = null}={}) => {
  var db, usersStore, projectsStore, articlesStore, mediaStore;  
  
  
    const init = () => {
  
      window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || 
      window.msIndexedDB;
       
      window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || 
      window.msIDBTransaction;
      window.IDBKeyRange = window.IDBKeyRange || 
      window.webkitIDBKeyRange || window.msIDBKeyRange
       
      if (!window.indexedDB) {
         window.alert("Your browser doesn't support a stable version of IndexedDB.")
      }
      const userData = [
        { id: "01", name: "Gopal K Varma", age: 35, email: "contact@tutorialspoint.com" },
        { id: "02", name: "Prasad", age: 24, email: "prasad@tutorialspoint.com" }
      ];


      var request = window.indexedDB.open("test", 1);
      
      request.onerror = function(event) {
         console.log("error: ");
      };
      
      request.onsuccess = function(event) {
         db = request.result;
         console.log("success: ");
         console.log(db);
      };
      
      request.onupgradeneeded = function(event) {
         db = event.target.result;

        //  db.deleteObjectStore('user');
        //  console.log(db);
         var objectStore = db.createObjectStore("user", {keyPath: "id"});
         
         for (var i in userData) {
            objectStore.add(userData[i]);
         }
      }

      onDBInit();

      

    }
    const read = () => {
      var transaction = db.transaction(["user"]);
      var objectStore = transaction.objectStore("user");
      var request = objectStore.get("00-03");
      
      request.onerror = function(event) {
         alert("Unable to retrieve daa from database!");
      };
      
      request.onsuccess = function(event) {
         // Do something with the request.result!
         if(request.result) {
            alert("Name: " + request.result.name + ", Age: " + request.result.age + ", Email: " + request.result.email);
         } else {
            alert("Kenny couldn't be found in your database!");
         }
      };
   }
   
   const readAll=()=> {
      var objectStore = db.transaction("user").objectStore("user");
      
      objectStore.openCursor().onsuccess = function(event) {
         var cursor = event.target.result;
         
         if (cursor) {
            alert("Name for id " + cursor.key + " is " + cursor.value.name + ", Age: " + cursor.value.age + ", Email: " + cursor.value.email);
            cursor.continue();
         } else {
            alert("No more entries!");
         }
      };
   }
    const add = () => {
      var request = db.transaction(["user"], "readwrite")
      .objectStore("user")
      // .add({ id: "01", name: "prasad", age: 24, email: "prasad@tutorialspoint.com" });
      .add({ id: "00-03", name: "Kenny", age: 2, email: "kenny@tutorialspoint.com" });
      
      request.onsuccess = function(event) {
         alert("Kenny has been added to your database.");
      };
      
      request.onerror = function(event) {
         alert("Unable to add data\r\nPrasad is already exist in your database! ");
      }
   }
   const remove = () => {
    var request = db.transaction(["user"], "readwrite")
    .objectStore("user")
    .delete("00-03");
    
    request.onsuccess = function(event) {
       alert("Kenny's entry has been removed from your database.");
    };
 }
    return {init,add,read,readAll,remove};
}

exports.DBManager=DBManager;



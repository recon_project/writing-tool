window.addEventListener('DOMContentLoaded', () => {
    let buttonTest=document.querySelector('.test-ipc');
    buttonTest.addEventListener('click', e=>{
        e.preventDefault();
        window.api.send("toMain", "some data").then(({event,data})=>{
                console.log(`Received ${data} from main process`);
          
        })
    })

    let buttonReadAll=document.querySelector('.read-all');
    buttonReadAll.addEventListener('click', e=>{
        e.preventDefault();

        window.db.readAll();
    })

    let buttonAddUser=document.querySelector('.add-user');
    buttonAddUser.addEventListener('click', e=>{
        e.preventDefault();
        
            // console.log(data);
            // console.log(`Received ${data} from main process`);
        window.db.add();
    })

    let buttonRemoveUser=document.querySelector('.remove-user');
    buttonRemoveUser.addEventListener('click', e=>{
        e.preventDefault();
        
            // console.log(data);
            // console.log(`Received ${data} from main process`);
        window.db.remove();
    })


    
});
const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const userDataPath = app.getPath('userData')




function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {

      preload: path.join(__dirname, 'preload.js')
    }
  })

  win.loadFile('index.html')

}

app.whenReady().then(() => {
  createWindow()


  ipcMain.on("toMain", (event, args) => {
    // Send result back to renderer process
    console.log('main received msg');
    console.log(args);
    event.sender.send("fromMain", userDataPath);
  
  });
  

  
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
  
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


# writing tool

A collaborative writing tool using git for versioning / offline work / conflict management
# command to start a development server
` npm start`
# commands to package / build executable for windows / linux
(for win32 build, must install wine and mono)
1. ## with electron-forge (must configure package.json accordingly)
This method is too slow for creating win32 build from a linux machine, we'd better use electron-builder described dowm below ...
` npm run make`
` npm run make -- --platform=win32`

### in package.json
```
  "build": {
    "appId": "com.lavillahermosa.writing-tool",
    "win": {
      "target": "squirrel",
      "icon": "build/icon.ico"
    }
  },
  "config": {
    "forge": {
      "packagerConfig": {},
      "makers": [
        {
          "name": "@electron-forge/maker-squirrel",
          "config": {
            "name": "writing_tool"
          }
        },
        {
          "name": "@electron-forge/maker-zip",
          "platforms": [
            "darwin"
          ]
        },
        {
          "name": "@electron-forge/maker-deb",
          "config": {}
        },
        {
          "name": "@electron-forge/maker-rpm",
          "config": {}
        }
      ]
    }
  }
```
Other ways to build a windows installer exist

2. ## with electron-builder (must configure package.json accordingly)
` npm run dist `
### in package.json 
```
  "scripts": {
    ...
    "make": "electron-forge make",
    "pack": "electron-builder --dir",
    "dist": "electron-builder -wl"
  },
```
For mac, impossible to build from neither win32 or linux platform (electron-builder -w*m*l)
## with docker
``` 
docker run --rm -ti \
 --env-file <(env | grep -iE 'DEBUG|NODE_|ELECTRON_|YARN_|NPM_|CI|CIRCLE|TRAVIS_TAG|TRAVIS|TRAVIS_REPO_|TRAVIS_BUILD_|TRAVIS_BRANCH|TRAVIS_PULL_REQUEST_|APPVEYOR_|CSC_|GH_|GITHUB_|BT_|AWS_|STRIP|BUILD_') \
 --env ELECTRON_CACHE="/root/.cache/electron" \
 --env ELECTRON_BUILDER_CACHE="/root/.cache/electron-builder" \
 -v ${PWD}:/project \
 -v ${PWD##*/}-node-modules:/project/node_modules \
 -v ~/.cache/electron:/root/.cache/electron \
 -v ~/.cache/electron-builder:/root/.cache/electron-builder \
 electronuserland/builder:wine
 ```

And then in docker 
```
yarn && yarn dist
```

const {
  contextBridge,
  ipcRenderer
} = require("electron");

var Mustache = require('mustache');

// require models
const dbModule = require('./src/modules/DBManager');

var db;

//require controllers
const articleListController = require('./src/controllers/articleListController');

// does this rendering function has to be in a "manager" ex: ExplorerManager or in a renderer (controller)?
function renderHello() {
  var template = document.getElementById('template').innerHTML;
  var rendered = Mustache.render(template, { name: 'Luke' });
  document.getElementById('target').innerHTML = rendered;
}
  
db=dbModule.DBManager({onDBInit:()=>{
    console.log('onDBInit called');
}});

db.init();

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object
contextBridge.exposeInMainWorld(
  "api", {
      send: (channel, data) => {
          // whitelist channels
          let validChannels = ["toMain"];
          if (validChannels.includes(channel)) {
            console.log(data);
              ipcRenderer.send(channel, data);
          }
          return new Promise((resolve) =>
          ipcRenderer.once('fromMain', (event, data) => resolve({ event, data }))
    )
      },
      receive: (channel, func) => {
          let validChannels = ["fromMain"];
          if (validChannels.includes(channel)) {
              // Deliberately strip event as it includes `sender` 
              console.log(channel)
              ipcRenderer.on(channel, () => {
                console.log('huh');
                
              
              });
          }
      }
  }
);
contextBridge.exposeInMainWorld(
  "db", {
      readAll: () => {
          // whitelist channels
          db.readAll();
      },
      add: () => {
          db.add();
      },
      remove: () => {
          db.remove();
      }
    }   
);
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
      const element = document.getElementById(selector)
      if (element) element.innerText = text
    }
  
    for (const type of ['chrome', 'node', 'electron']) {
      replaceText(`${type}-version`, process.versions[type])
    }

    renderHello();

  })
  